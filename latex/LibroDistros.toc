\selectlanguage *{spanish}
\contentsline {part}{I\hspace {1em}Introducción}{6}{part.1}%
\contentsline {chapter}{\numberline {1}Una historia sobre distros}{7}{chapter.1}%
\contentsline {chapter}{\numberline {2}¿Por qué usar Linux?}{9}{chapter.2}%
\contentsline {chapter}{\numberline {3}Sobre distros y escritorios}{12}{chapter.3}%
\contentsline {chapter}{\numberline {4}Consejos para la instalación}{16}{chapter.4}%
\contentsline {part}{II\hspace {1em}Sobre las Distros}{19}{part.2}%
\contentsline {chapter}{\numberline {5}Introducción a la segunda parte}{20}{chapter.5}%
\contentsline {chapter}{\numberline {6}Debian}{24}{chapter.6}%
\contentsline {chapter}{\numberline {7}Ubuntu}{30}{chapter.7}%
\contentsline {chapter}{\numberline {8}Fedora}{37}{chapter.8}%
\contentsline {chapter}{\numberline {9}Xubuntu}{44}{chapter.9}%
\contentsline {chapter}{\numberline {10}Manjaro}{51}{chapter.10}%
\contentsline {chapter}{\numberline {11}Mint MATE}{64}{chapter.11}%
\contentsline {chapter}{\numberline {12}Huayra}{76}{chapter.12}%
\contentsline {chapter}{\numberline {13}Trisquel}{84}{chapter.13}%
\contentsline {chapter}{\numberline {14}Preguntas Frecuentes}{92}{chapter.14}%
\contentsline {chapter}{\numberline {15}Glosario.}{98}{chapter.15}%
